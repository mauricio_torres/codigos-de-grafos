
import java.util.ArrayList;

public class Nodo{

    private ArrayList<Arco> Adyacente;

    private String Nombre;

    public Nodo (String Name) {
        Nombre = Name;
        Adyacente = new ArrayList<Arco>();
    }
 
    public String getNombre () {
        return Nombre;
    }
 
    public void setNombre (String val) {
        this.Nombre = val;
    }

    public void ModificarAdyacente (Nodo Origen, Nodo Destino, Nodo Nuevo, Integer NuevoPeso) {
        for (int i = 0; i < Origen.Adyacente.size(); i++) {
                if(Origen.Adyacente.get(i).equals(Destino)){                    
                    Origen.Adyacente.remove(i);
                    Arco A= new Arco(Origen, Destino, NuevoPeso);
                    Origen.Adyacente.add(A);
                } 
        }    
    }

    public void AgregarAdyacente (Nodo Origen, Nodo Destino, Integer Peso) {
        Arco A= new Arco(Origen, Destino, Peso);
        Adyacente.add(A);        
    }

    public void EliminarAdyacente (Nodo Origen, Nodo Destino) {
        if(EsAdyacentes(Origen, Destino)==true){
            for (int i = 0; i < Origen.Adyacente.size(); i++) {
                if(Origen.Adyacente.get(i).equals(Destino)){
                  Origen.Adyacente.remove(i);                              
                } 
            }
	
        }
    }

    public ArrayList getAdyacente () {
        return Adyacente;
    }

    public void setAdyacente (ArrayList val) {
        this.Adyacente = val;
    }

    private void ListarAdyacentes (Nodo Nodo) {
        for (int i = 0; i < Nodo.Adyacente.size(); i++) {
			System.out.println(Adyacente.get(i));
	}
    }

    public boolean EsAdyacentes (Nodo Origen, Nodo Destino) {
        for (int i = 0; i < Origen.Adyacente.size(); i++) {
            if(Adyacente.get(i).equals(Destino)){
                  return true;                              
            } 
	}
        return false;
    }

    private void Renombrar (String Nuevo) {
        this.Nombre=Nuevo;
    }

}

