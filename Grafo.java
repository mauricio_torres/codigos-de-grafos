
import java.util.LinkedList;

public class Grafo{

    private Nodo Nodos;

    private java.util.List<Nodo> nodos = new LinkedList<Nodo>();

    void AgregarNodo(String nombreNodo){
        Nodo n = new Nodo(nombreNodo);
        nodos.add(n);
    }
    
    public Grafo () {
 
    }

    public Nodo getNodos () {
        return Nodos;
    }

    public void setNodos (Nodo val) {
        this.Nodos = val;
    }

    private void EliminarNodo (Nodo nod) {
        nod = null;
    }

}

