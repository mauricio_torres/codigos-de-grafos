public class Arco {
 
    private Nodo Origen;

    private Nodo Destino;

    private int Peso;

    public Arco (Nodo Orige, Nodo Destin, Integer Pes) {
        Origen = Orige;
        Destino = Destin;
        Peso = Pes;
    }

    public Nodo getDestino () {
        return Destino;
    }

    public void setDestino (Nodo val) {
        this.Destino = val;
    }

    public Nodo getOrigen () {
        return Origen;
    }
 
    public void setOrigen (Nodo val) {
        this.Origen = val;
    }

    public int getPeso () {
        return Peso;
    }

    public void setPeso (int val) {
        this.Peso = val;
    }

    private void ActualizarPeso (Arco Arc, int Pes) {
        Arc.Peso = Pes;
    }

}

